(function() {
  'use strict';
  console.log('\'Allo \'Allo! Popup');

  chrome.runtime.getBackgroundPage(function(bg) {
    return console.log('Data from background --exi ' + bg.data);
  });

  angular.module('LightsApp', ['ngResource', 'ui.bootstrap-slider']).controller('LightsCtrl', [
    "$scope", "Ligths", function($scope, Ligths) {
      $scope.levels = {
        general: 0,
        panel1: false,
        panel2: false,
        panel3: false,
        panel4: false,
        panel5: false,
        panel6: false,
        panel7: false,
        panel8: false
      };
      $scope.options = {
        host: ''
      };
      chrome.storage.sync.get({
        host: 'http://ci.lan:8080/'
      }, function(items) {
        console.log(items.ip);
        return $scope.$apply(function() {
          return $scope.options.host = items.host;
        });
      });
      return $scope.click = function(value) {
        console.log('Hi');
        return Ligths.post({
          host: '192.168.1.200:8081'
        }, {
          command: value,
          level: 127
        });
      };
    }
  ]).factory('Ligths', [
    '$resource', function($resource) {
      return $resource('http://:host/levels', {
        host: '192.168.1.200:8080'
      }, {
        get: {
          method: 'GET'
        },
        post: {
          method: 'POST',
          url: 'http://:host/setLevel'
        }
      });
    }
  ]);

}).call(this);
