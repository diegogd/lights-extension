(function() {
  'use strict';
  console.log('\'Allo \'Allo! Option');

  angular.module('OptionsApp', []).controller('OptionsCtrl', [
    '$scope', function($scope) {
      $scope.options = {
        host: ''
      };
      chrome.storage.sync.get({
        host: 'http://ci.lan:8080/'
      }, function(items) {
        return $scope.$apply(function() {
          return $scope.options.host = items.host;
        });
      });
      return $scope.save = function() {
        return chrome.storage.sync.set($scope.options, function() {
          return $scope.message = 'Settings saved';
        });
      };
    }
  ]);

}).call(this);
